import App from './App.svelte';
import { getItem, getItemOld, saveItem } from './storage';
import { defaultCategories, defaultIncomes } from './defaults';
import RemoteStorage from 'remotestoragejs';
import Widget from 'remotestorage-widget';
import type { RSChangeEvent } from './types';

const INCOMES_KEY = 'incomes';
const CATEGORIES_KEY = 'categories';
const RS_APP_NAME = 'crooke.simple-budget';

const app = new App({
	target: document.querySelector('#app'),
	props: {
		incomes: defaultIncomes,
		categories: defaultCategories,
		buildDate: __BUILD_DATE__
	}
});

const remoteStorage = new RemoteStorage({ logging: !__PRODUCTION__ });
remoteStorage.access.claim(RS_APP_NAME, 'rw');
remoteStorage.caching.enable(`/${RS_APP_NAME}/`);

const widget = new Widget(remoteStorage);
widget.attach('blockstack');

const client = remoteStorage.scope(`/${RS_APP_NAME}/`);

client.declareType(INCOMES_KEY, { type: 'array' });
client.declareType(CATEGORIES_KEY, { type: 'array' });

client.on('change', event => {
	let changeEvent = event as RSChangeEvent;
	if (changeEvent.origin === 'remote') {
		if ([INCOMES_KEY, CATEGORIES_KEY].includes(changeEvent.relativePath)) {
			console.log('Syncing local app with data from remote app');
			const obj = {};
			obj[changeEvent.relativePath] = changeEvent.newValue;
			app.$set(obj);
		}
	}
});

app.$on('categories', e => saveItem(CATEGORIES_KEY, e.detail, client));
app.$on('incomes', e => saveItem(INCOMES_KEY, e.detail, client));

async function loadSavedData() {
	let savedIncome = await getItem(INCOMES_KEY, client);

	// This is to migrate data stored the old way, if it exists, to new storage
	if (!savedIncome) savedIncome = getItemOld(INCOMES_KEY);

	if (savedIncome) app.$set({ incomes: savedIncome });

	let savedCategories = await getItem(CATEGORIES_KEY, client);

	// This is to migrate data stored the old way, if it exists, to new storage
	if (!savedCategories) savedCategories = getItemOld(CATEGORIES_KEY);

	if (savedCategories) app.$set({ categories: savedCategories });
}

// TODO: maybe should disable the app until this finishes
loadSavedData();

export default app;
