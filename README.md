# Budget App

Simple single page browser app for figuring out what your monthly budget should be based on your income.

## Get started

Install the dependencies...

```bash
git clone https://gitlab.com/crooke/budget-app.git
cd budget-app
npm install
```

...then start webpack:

```bash
npm run dev
```

> TODO: run on HTTPS?

Navigate to [127.0.0.1:5555](http://127.0.0.1:5555). You should see your app running. Edit a component file in `src`, save it, and the page should reload with your changes.

## Configuration

If the app will run on a path other than the root, you should set the `BASE_PATH` environment variable when running webpack.
Be sure to include the `/` before but not after, e.g. `/budget-app`.
Refer to the webpack.config.js for more info.
