/// <reference types="svelte" />

// These get replaced by rollup at build-time
const __PRODUCTION__: boolean;
const __BUILD_DATE__: string;
const __BASE_PATH__: string;
