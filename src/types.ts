export interface Income {
	name: string;
	hourly: number;
	hoursPerWeek: number;
	weeksPerYear: number;
}

export interface Category {
	name: string;
	percentage: number;
	items: CategoryItem[];
}

export interface CategoryItem {
	name: string;
	value?: number;
}

export interface RSChangeEvent {
	/** Absolute path of the changed node, from the storage root */
	path: string;
	/** Path of the changed node, relative to this baseclient's scope root */
	relativePath: string;
	/** See origin descriptions below */
	origin: 'window' | 'local' | 'remote' | 'conflict';
	/** Old body of the changed node (local version in conflicts; undefined if creation) */
	oldValue: unknown | undefined;
	/** New body of the changed node (remote version in conflicts; undefined if deletion) */
	newValue: unknown | undefined;
	/** Old contentType of the changed node (local version for conflicts; undefined if creation) */
	oldContentType: string | undefined;
	/** New contentType of the changed node (remote version for conflicts; undefined if deletion) */
	newContentType: string | undefined;
}
