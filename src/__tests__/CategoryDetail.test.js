import '@testing-library/jest-dom/extend-expect';
import { render, fireEvent } from '@testing-library/svelte';
import CategoryDetail from '../CategoryDetail.svelte';

const ITEMS = [
	{ name: 'Pizza', value: 25 },
	{ name: 'Donuts', value: 33 },
	{ name: 'Cookies' }
];

test('inputs display correct value on initial render', () => {
	const c = render(CategoryDetail, { items: ITEMS });
	const amounts = c.getAllByPlaceholderText('Amount');

	expect(amounts.length).toBe(3);

	for (var i = 0; i < ITEMS.length; i++) {
		expect(amounts[i].value).toBe(String(ITEMS[i].value || 0));
	}
});

test('total is correct - before input', () => {
	const c = render(CategoryDetail, {
		name: 'Retirement',
		items: ITEMS
	});
	const total = c.getByRole('cell', { name: 'Retirement Total' });
	expect(total.textContent).toBe('$58');
});

test('total is correct - after input', async () => {
	const c = render(CategoryDetail, { name: 'Recreation', items: ITEMS });
	const amounts = c.getAllByPlaceholderText('Amount');

	for (var i = amounts.length - 1; i >= 0; i--) {
		await fireEvent.input(amounts[i], { target: { value: 1234 } });
	}

	const total = c.getByRole('cell', { name: 'Recreation Total' });

	expect(total.textContent).toBe(String(`$${1234 * amounts.length}`));
});

test('total warns of danger when it exceeds the goal', () => {
	const c = render(CategoryDetail, { name: 'Food', items: ITEMS, goal: 3 });
	const total = c.getByRole('cell', { name: 'Food Total' });
	expect(total.className).toBe('alert alert-danger');
});
