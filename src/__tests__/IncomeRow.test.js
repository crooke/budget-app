require('@testing-library/jest-dom/extend-expect');
const t = require('@testing-library/svelte');
const IncomeRow = require('../IncomeRow.svelte');

const HOURS_PER_WEEK = 40;
const WEEKS_PER_YEAR = 52;
const MONTHS_PER_YEAR = 12;

test('renders correctly', () => {
	const { getByPlaceholderText } = t.render(IncomeRow, {
		name: 'George'
	});
	expect(getByPlaceholderText('Hourly')).toBeInTheDocument();
	expect(getByPlaceholderText('Monthly')).toBeInTheDocument();
	expect(getByPlaceholderText('Yearly')).toBeInTheDocument();
});

test('monthly and yearly inputs change when hourly changes', async () => {
	const { getByPlaceholderText } = t.render(IncomeRow);
	const hourly = getByPlaceholderText('Hourly');
	const monthly = getByPlaceholderText('Monthly');
	const yearly = getByPlaceholderText('Yearly');

	const income = 30;
	await t.fireEvent.input(hourly, { target: { value: income } });

	expect(hourly.value).toBe(String(income));
	expect(yearly.value).toBe(
		String(Math.round(income * HOURS_PER_WEEK * WEEKS_PER_YEAR))
	);
	expect(monthly.value).toBe(
		String(
			Math.round(
				(income * HOURS_PER_WEEK * WEEKS_PER_YEAR) / MONTHS_PER_YEAR
			)
		)
	);
});

test('hourly and yearly inputs change when monthly changes', async () => {
	const { getByPlaceholderText } = t.render(IncomeRow);
	const hourly = getByPlaceholderText('Hourly');
	const monthly = getByPlaceholderText('Monthly');
	const yearly = getByPlaceholderText('Yearly');

	const income = 4837;
	await t.fireEvent.input(monthly, { target: { value: income } });

	expect(monthly.value).toBe(String(Math.round(income)));
	expect(yearly.value).toBe(String(Math.round(income * MONTHS_PER_YEAR)));
	expect(hourly.value).toBe(
		String(
			Math.round(
				(income * MONTHS_PER_YEAR) / WEEKS_PER_YEAR / HOURS_PER_WEEK
			)
		)
	);
});

test('monthly and hourly inputs change when yearly changes', async () => {
	const { getByPlaceholderText } = t.render(IncomeRow);
	const hourly = getByPlaceholderText('Hourly');
	const monthly = getByPlaceholderText('Monthly');
	const yearly = getByPlaceholderText('Yearly');

	const income = 67000;
	await t.fireEvent.input(yearly, { target: { value: income } });

	expect(yearly.value).toBe(String(income));
	expect(hourly.value).toBe(
		String(Math.round(income / HOURS_PER_WEEK / WEEKS_PER_YEAR))
	);
	expect(monthly.value).toBe(String(Math.round(income / MONTHS_PER_YEAR)));
});
