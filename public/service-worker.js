importScripts(
	'https://storage.googleapis.com/workbox-cdn/releases/5.1.2/workbox-sw.js'
);

if (workbox) {
	workbox.routing.registerRoute(
		'/budget-app/',
		new workbox.strategies.NetworkFirst()
	);
	workbox.routing.registerRoute(/\.js$/, new workbox.strategies.NetworkFirst());
	workbox.routing.registerRoute(
		/\.css$/,
		new workbox.strategies.NetworkFirst()
	);
}
