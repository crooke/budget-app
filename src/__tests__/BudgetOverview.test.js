import '@testing-library/jest-dom/extend-expect';
import { render, fireEvent } from '@testing-library/svelte';
import BudgetOverview from '../BudgetOverview.svelte';

const CATEGORIES = [
	{ name: 'Savings', percentage: 25 },
	{ name: 'Donuts', percentage: 33 },
	{ name: 'Cars', percentage: 29 }
];

test('renders correctly', () => {
	const c = render(BudgetOverview, { income: 1000, categories: CATEGORIES });
	const percentages = c.getAllByPlaceholderText('Percent');
	const budgets = c.getAllByPlaceholderText('Budget');

	expect(percentages.length).toBe(3);
	expect(budgets.length).toBe(3);

	for (var i = 0; i < CATEGORIES.length; i++) {
		expect(percentages[i].value).toBe(String(CATEGORIES[i].percentage));
		expect(budgets[i].value).toBe(
			String(1000 * (CATEGORIES[i].percentage / 100))
		);
	}
});

test('percentage total is correct - before input', () => {
	const c = render(BudgetOverview, { income: 0, categories: CATEGORIES });
	const total = c.getByRole('cell', { name: 'Percentage Total' });
	expect(total.textContent).toBe('87%');
});

test('percentage total is correct - after input (even w/no income)', async () => {
	const c = render(BudgetOverview, { income: 0, categories: CATEGORIES });
	const percentages = c.getAllByPlaceholderText('Percent');

	for (var i = percentages.length - 1; i >= 0; i--) {
		await fireEvent.input(percentages[i], { target: { value: 13 } });
	}

	const total = c.getByRole('cell', { name: 'Percentage Total' });
	expect(total.textContent).toBe(String(13 * percentages.length + '%'));
});

test('budget total is correct - before input', () => {
	const c = render(BudgetOverview, { income: 1234, categories: CATEGORIES });
	const total = c.getByRole('cell', { name: 'Budget Total' });
	expect(total.textContent).toBe('$1074');
});

test('budget total is correct - after input', async () => {
	const c = render(BudgetOverview, { income: 5200, categories: CATEGORIES });
	const budgets = c.getAllByPlaceholderText('Budget');

	for (var i = budgets.length - 1; i >= 0; i--) {
		await fireEvent.input(budgets[i], { target: { value: 1234 } });
	}

	const total = c.getByRole('cell', { name: 'Budget Total' });

	expect(total.textContent).toBe(String(`$${1234 * budgets.length}`));
});

test('budgets update in response to income changes', async () => {
	const c = render(BudgetOverview, { income: 1000, categories: CATEGORIES });

	const total = c.getByRole('cell', { name: 'Budget Total' });
	expect(total.textContent).toBe('$870');

	await c.component.$set({ income: 2000, categories: CATEGORIES });

	expect(total.textContent).toBe('$1740');
});

test('percentages do NOT change in response to income changes', async () => {
	const c = render(BudgetOverview, { income: 1000, categories: CATEGORIES });

	const total = c.getByRole('cell', { name: 'Percentage Total' });
	expect(total.textContent).toBe('87%');

	await c.component.$set({ income: 2000, categories: CATEGORIES });

	expect(total.textContent).toBe('87%');
});
