import '@testing-library/jest-dom/extend-expect';
import { render, fireEvent } from '@testing-library/svelte';
import NumInput from '../NumInput.svelte';

test('renders correctly', () => {
	const c = render(NumInput, {
		id: 'asdf',
		name: 'my-input',
		placeholder: 'Enter here'
	});

	const input = c.getByPlaceholderText('Enter here');
	expect(c.container.querySelector('#asdf')).toBeDefined();
	expect(c.container.querySelector('#asdf[name=my-input]')).toBeDefined();
});

test('default value is rounded', () => {
	const c = render(NumInput, {
		placeholder: 'Enter here',
		value: 3.33
	});

	const input = c.getByPlaceholderText('Enter here');
	expect(input.value).toBe('3');
});

test('value updates correctly', async () => {
	const c = render(NumInput, {
		placeholder: 'Enter here'
	});

	const input = c.getByPlaceholderText('Enter here');
	await fireEvent.input(input, { target: { value: 34.98 } });
	expect(input.value).toBe('35');
});

test('component fires input event', async () => {
	// Create handler function
	const handler = jest.fn();

	// Render component
	const c = render(NumInput, {
		placeholder: 'Enter here',
		name: 'my-input'
	});

	// Register event handler on component (not sure how to pass in as prop)
	c.component.$on('input', handler);

	// Fire the component's event
	const input = c.getByPlaceholderText('Enter here');
	await fireEvent.input(input, { target: { value: 34.98 } });

	expect(handler).nthCalledWith(
		1,
		expect.objectContaining({
			detail: {
				name: 'my-input',
				value: 35
			}
		})
	);
});
