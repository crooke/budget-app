import '@testing-library/jest-dom/extend-expect';
import { render, fireEvent } from '@testing-library/svelte';
import Income from '../Income.svelte';

const HOURS_PER_WEEK = 40;
const WEEKS_PER_YEAR = 52;
const MONTHS_PER_YEAR = 12;

const incomes = [
	{
		name: 'Income 1',
		hourly: 0,
		hoursPerWeek: 40,
		weeksPerYear: 52
	}
];

test('renders correctly', () => {
	const { getByRole, getAllByPlaceholderText } = render(Income);

	expect(getAllByPlaceholderText('Hourly').length).toBe(2);
	expect(getAllByPlaceholderText('Monthly').length).toBe(2);
	expect(getAllByPlaceholderText('Yearly').length).toBe(2);

	const total = getByRole('cell', { name: 'Hourly Total' });
	expect(total.textContent).toBe('$0');
});

test('initial render computes incomes off hourly', () => {
	const r = render(Income, {
		incomes: [
			{ name: 'A', hourly: 10, hoursPerWeek: 40, weeksPerYear: 52 },
			{ name: 'B', hourly: 20, hoursPerWeek: 30, weeksPerYear: 50 }
		]
	});

	// 10 * 40 * 52 = 20800/year
	// 20 * 30 * 50 = 30000/year
	// 50800/12 = 4233.33/month

	const hourlyTotal = r.getByRole('cell', { name: 'Hourly Total' });
	const monthlyTotal = r.getByRole('cell', { name: 'Monthly Total' });
	const yearlyTotal = r.getByRole('cell', { name: 'Yearly Total' });

	expect(hourlyTotal.textContent).toBe('$30');
	expect(monthlyTotal.textContent).toBe('$4233');
	expect(yearlyTotal.textContent).toBe('$50800');
});

test('computes hourly total correcly', async () => {
	const { getByRole, getAllByPlaceholderText } = render(Income);
	const hours = getAllByPlaceholderText('Hourly');

	const income = 30;
	for (var i = hours.length - 1; i >= 0; i--) {
		await fireEvent.input(hours[i], { target: { value: income } });
	}

	const total = getByRole('cell', { name: 'Hourly Total' });
	expect(total.textContent).toBe(`$${income * 2}`);
});

test('computes monthly total correcly', async () => {
	const { getByRole, getAllByPlaceholderText } = render(Income);
	const hours = getAllByPlaceholderText('Monthly');

	const income = 3839;
	for (var i = hours.length - 1; i >= 0; i--) {
		await fireEvent.input(hours[i], { target: { value: income } });
	}

	const total = getByRole('cell', { name: 'Monthly Total' });
	expect(total.textContent).toBe(`$${income * 2}`);
});

test('computes yearly total correcly', async () => {
	const { getByRole, getAllByPlaceholderText } = render(Income);
	const hours = getAllByPlaceholderText('Yearly');

	const income = 54039;
	for (var i = hours.length - 1; i >= 0; i--) {
		await fireEvent.input(hours[i], { target: { value: income } });
	}

	const total = getByRole('cell', { name: 'Yearly Total' });
	expect(total.textContent).toBe(`$${income * 2}`);
});

test('monthly and yearly inputs change when hourly changes', async () => {
	const { getByPlaceholderText } = render(Income, { incomes });
	const hourly = getByPlaceholderText('Hourly');
	const monthly = getByPlaceholderText('Monthly');
	const yearly = getByPlaceholderText('Yearly');

	const income = 30;
	await fireEvent.input(hourly, { target: { value: income } });

	expect(hourly.value).toBe(String(income));
	expect(yearly.value).toBe(
		String(Math.round(income * HOURS_PER_WEEK * WEEKS_PER_YEAR))
	);
	expect(monthly.value).toBe(
		String(
			Math.round(
				(income * HOURS_PER_WEEK * WEEKS_PER_YEAR) / MONTHS_PER_YEAR
			)
		)
	);
});

test('hourly and yearly inputs change when monthly changes', async () => {
	const { getByPlaceholderText } = render(Income, { incomes });
	const hourly = getByPlaceholderText('Hourly');
	const monthly = getByPlaceholderText('Monthly');
	const yearly = getByPlaceholderText('Yearly');

	const income = 4837;
	await fireEvent.input(monthly, { target: { value: income } });

	expect(monthly.value).toBe(String(Math.round(income)));
	expect(yearly.value).toBe(String(Math.round(income * MONTHS_PER_YEAR)));
	expect(hourly.value).toBe(
		String(
			Math.round(
				(income * MONTHS_PER_YEAR) / WEEKS_PER_YEAR / HOURS_PER_WEEK
			)
		)
	);
});

test('monthly and hourly inputs change when yearly changes', async () => {
	const { getByPlaceholderText } = render(Income, { incomes });
	const hourly = getByPlaceholderText('Hourly');
	const monthly = getByPlaceholderText('Monthly');
	const yearly = getByPlaceholderText('Yearly');

	const income = 67000;
	await fireEvent.input(yearly, { target: { value: income } });

	expect(yearly.value).toBe(String(income));
	expect(hourly.value).toBe(
		String(Math.round(income / HOURS_PER_WEEK / WEEKS_PER_YEAR))
	);
	expect(monthly.value).toBe(String(Math.round(income / MONTHS_PER_YEAR)));
});
