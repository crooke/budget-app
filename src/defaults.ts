import type { Category, Income } from './types';

export const defaultIncomes: Income[] = [
	{
		name: 'Income 1',
		hourly: 10,
		hoursPerWeek: 40,
		weeksPerYear: 52,
	},
	{
		name: 'Income 2',
		hourly: 0,
		hoursPerWeek: 40,
		weeksPerYear: 52,
	},
];

export const defaultCategories: Category[] = [
	{
		name: 'Taxes',
		percentage: 22,
		items: [
			{ name: 'Income 1', value: 0 },
			{ name: 'Income 2', value: 0 },
		],
	},
	{
		name: 'Giving',
		percentage: 10,
		items: [{ name: 'Church' }, { name: 'Charity' }],
	},
	{
		name: 'Retirement Savings',
		percentage: 15,
		items: [{ name: '401K' }, { name: 'Roth IRA' }],
	},
	{
		name: 'General Savings',
		percentage: 5,
		items: [{ name: 'House' }, { name: 'Car' }],
	},
	{
		name: 'Housing',
		percentage: 25,
		items: [
			{ name: 'Mortgage' },
			{ name: 'Insurance' },
			{ name: 'Gas' },
			{ name: 'Electric' },
			{ name: 'Water' },
			{ name: 'Trash' },
			{ name: 'Internet' },
		],
	},
	{
		name: 'Food',
		percentage: 8,
		items: [{ name: 'Groceries' }, { name: 'Restaurants' }],
	},
	{
		name: 'Transportation',
		percentage: 5,
		items: [
			{ name: 'Car Payment' },
			{ name: 'Gas' },
			{ name: 'Maintenance' },
			{ name: 'Taxes & Registration' },
			{ name: 'Insurance' },
			{ name: 'Oil changes' },
			{ name: 'Tires' },
		],
	},
	{
		name: 'Health & Wellness',
		percentage: 6,
		items: [
			{ name: 'Health Insurance' },
			{ name: 'Dental Insurance' },
			{ name: 'Vision Insurance' },
			{ name: 'Haircuts' },
		],
	},
	{
		name: 'Recreation',
		percentage: 3,
		items: [{ name: 'Movies' }, { name: 'Netflix' }, { name: 'Spotify' }],
	},
	{ name: 'Miscellaneous', percentage: 1, items: [{ name: 'Pet Care' }] },
];
