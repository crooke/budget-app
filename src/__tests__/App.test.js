import '@testing-library/jest-dom/extend-expect';
import { render, fireEvent } from '@testing-library/svelte';
import App from '../App.svelte';

const INCOMES = [
	{
		name: 'A',
		hourly: 25,
		hoursPerWeek: 40,
		weeksPerYear: 52
	}
];

const CATEGORIES = [
	{
		name: 'Savings',
		percentage: 25,
		items: [{ name: 'House' }, { name: 'Car' }]
	}
];

test('incomes are rendered on initial render', () => {
	const c = render(App, { incomes: INCOMES, categories: CATEGORIES });
	const income = c.getByPlaceholderText('Hourly');
	expect(income.value).toBe('25');
});

test('budget overview and details update when income changes', async () => {
	const c = render(App, { incomes: INCOMES, categories: CATEGORIES });
	const incomes = c.getAllByPlaceholderText('Hourly');

	// Take a 'snapshot' of some Overview and Detail values before any input
	const overviewTotal = c.getByRole('cell', { name: 'Budget Total' });
	const originalOverviewTotal = overviewTotal.textContent;

	const detailGoal = c.getByRole('cell', { name: 'Savings Goal' });
	const originalDetailGoal = detailGoal.textContent;

	await fireEvent.input(incomes[0], { target: { value: 50 } });

	// Just check that the values changed -
	// component's unit tests will (should) make sure values are calculated correctly
	expect(overviewTotal.textContent).not.toBe(originalOverviewTotal);
	expect(detailGoal.textContent).not.toBe(originalDetailGoal);
});
