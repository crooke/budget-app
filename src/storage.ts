import type BaseClient from 'remotestoragejs/release/types/baseclient';

async function getItem<T>(key: string, client: BaseClient): Promise<T> {
	try {
		return (await client.getObject(key)) as T;
	} catch (err) {
		console.warn(`Error while trying to get '${key}' from storage`, err);
		return null;
	}
}

/**
 * Used to use local storage (and Blockstack) for storage.
 * This returns items stored in local storage for migration to new storage.
 */
function getItemOld(key: string): any {
	try {
		const storedJson = localStorage.getItem(key);
		return JSON.parse(storedJson);
	} catch (err) {
		console.warn(`Error while trying to get '${key}' from storage`, err);
		return null;
	}
}

async function saveItem(
	key: string,
	data: object,
	client: BaseClient
): Promise<void> {
	const jsonData = JSON.stringify(data);

	try {
		await client.storeObject(key, key, data);
	} catch (err) {
		console.warn(`Error storing '${key}'`, err, jsonData);
	}
}

export { getItem, saveItem, getItemOld };
